package com.twuc.webApp.domain;

import com.twuc.webApp.repository.OfficeRepository;
import com.twuc.webApp.repository.StaffRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

import javax.persistence.EntityManager;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest(showSql = false)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class BiDirectionalMappingTest {

  @Autowired
  private OfficeRepository officeRepository;

  @Autowired
  private StaffRepository staffRepository;

  @Autowired
  private EntityManager entityManager;

  @Test
  void should_create_office_and_staff() {
    Office office = new Office("office");
    Staff staff = new Staff("staff");

    staff.setOffice(office);
    office.addStaff(staff);

    officeRepository.save(office);
    entityManager.flush();
    entityManager.clear();

    Staff staffFound = staffRepository.findById(1L).orElseThrow(NoSuchElementException::new);
    assertEquals("office", staffFound.getOffice().getName());
  }
  @Test
  void should_update_staff_when_set_office_to_null() {
    Office office = new Office("office");
    Staff staff = new Staff("staff");

    staff.setOffice(office);
    office.addStaff(staff);

    officeRepository.save(office);
    entityManager.flush();
    entityManager.clear();

    Staff staffFound = staffRepository.findById(1L).orElseThrow(NoSuchElementException::new);
    staffFound.setOffice(null);
    entityManager.flush();
    assertNull(staffRepository.findById(1L).get().getOffice());
  }

  @Test
  void should_delete_staff_when_remove_from_office_side() {
    Office office = new Office("office");
    Staff staff = new Staff("staff");

    staff.setOffice(office);
    office.addStaff(staff);

    officeRepository.save(office);
    entityManager.flush();
    entityManager.clear();

    Office officeFound = officeRepository.findById(1L).orElseThrow(NoSuchElementException::new);
    officeFound.getStaffs().remove(0);
    entityManager.flush();
    assertFalse(staffRepository.findById(1L).isPresent());
  }
}
