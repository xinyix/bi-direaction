package com.twuc.webApp.domain;

import javax.persistence.*;

@Entity
public class Staff {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column
  private String name;

  @ManyToOne
  private Office office;

  public Staff() {
  }

  public Staff(String name) {
    this.name = name;
  }

  public void setOffice(Office office) {
    this.office = office;
  }

  public Office getOffice() {
    return office;
  }
}
