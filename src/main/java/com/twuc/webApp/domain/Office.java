package com.twuc.webApp.domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Office {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column
  private String name;

  @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "office")
  private List<Staff> staffs = new ArrayList<>();

  public Office() {
  }

  public String getName() {
    return name;
  }

  public Office(String name) {
    this.name = name;
  }

  public void addStaff(Staff staff){
    staffs.add(staff);
  }

  public void setStaffs(List<Staff> staffs) {
    this.staffs = staffs;
  }

  public List<Staff> getStaffs() {
    return staffs;
  }
}
